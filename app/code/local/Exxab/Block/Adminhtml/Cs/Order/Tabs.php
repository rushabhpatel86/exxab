<?php
/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */

class Exxab_Block_Adminhtml_Cs_Order_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_tab');
        $this->setDestElementId('grid_tab_content');
        $this->setTemplate('widget/tabshoriz.phtml');
    }

    /**
     * Prepare layout for dashboard bottom tabs
     *
     * To load block statically:
     *     1) content must be generated
     *     2) url should not be specified
     *     3) class should not be 'ajax'
     * To load with ajax:
     *     1) do not load content
     *     2) specify url (BE CAREFUL)
     *     3) specify class 'ajax'
     *
     * @return Mage_Adminhtml_Block_Dashboard_Grids
     */
    protected function _prepareLayout()
    {
        // load this active tab statically
        $this->addTab('pending_buyer', array(
            'label'     => $this->__('Pending Buyer'),
            'content'   => $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_pendingbuyer')->toHtml(),
            'active'    => true
        ));


        $this->addTab('pending_seller', array(
            'label'     => $this->__('Pending Seller'),
            'content'   => $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_pendingseller')->toHtml(),

        ));

        $this->addTab('hold', array(
            'label' => $this->__('Hold Order'),
            'content' => $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_hold')->toHtml(),
        ));

            $this->addTab('abandoned_order', array(
            'label' => $this->__('Abandoned Order'),
            'content' => $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_abandonedorder')->toHtml(),
        ));
        /*  $this->addTab('abandoned_order', array(
          'label' => $this->__('Abandoned Order'),
          'content' => $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_abandonedorder')->toHtml(),
          //'content' => $this->getLayout()->createBlock('Mage/adminhtml_block_report_shopcart_abandoned')->toHtml(),
          )); */
        return parent::_prepareLayout();
    }
}