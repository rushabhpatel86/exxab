<?php
/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */

class Exxab_Block_Adminhtml_Cs_Order_View extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'exxab';
        $this->_mode = 'view';
        $this->_controller = 'adminhtml_cs_order';
        //$this->setId('sales_order_view');

        //$this->_updateButton('save', 'label', Mage::helper('exxab')->__('Submit Comment'));
        $this->removeButton('save');
        $this->removeButton('delete');
    }

    public function getHeaderText()
    {
        return Mage::helper('exxab')->__("Order '%s'", $this->htmlEscape(Mage::registry('current_order')->getIncrementId()));
    }
}