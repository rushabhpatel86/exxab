<?php
/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */

class Exxab_Block_Adminhtml_Cs_Order_View_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{   
    public function __construct()
    {
        parent::__construct();
        $this->setId('cs_order_view_tabs');
        //$this->setDestElementId('cs_order_view_form');
        $this->setDestElementId('sales_order_view');
        $this->setTitle(Mage::helper('exxab')->__('Exxab CS | Order View'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('cs_order_form', array(
            'label'     => Mage::helper('exxab')->__('Information'),
            'title'     => Mage::helper('exxab')->__('Information'),
            'content'   => $this->getLayout()->createBlock('exxab/adminhtml_cs_order_view_tab_form')->toHtml(),
        ));

        $this->addTab('cs_order_history', array(
            'label'     => Mage::helper('exxab')->__('Comments History'),
            'url'       => $this->getUrl('*/*/orderhistory', array('_current' => true)),
            'class'     => 'ajax',
        ));

        return parent::_beforeToHtml();
    }
}