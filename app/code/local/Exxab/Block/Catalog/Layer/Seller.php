<?php
 
class Exxab_Block_Catalog_Layer_Seller extends Mage_Catalog_Block_Layer_View
{
 
    protected function _construct()
    {
        parent::_construct();

        /* To get picked up by Mage_Catalog_Product_List::getLayer() */
        Mage::register('current_layer', $this->getLayer(), true);
        
        
    }
 
    public function getLayer()
    {
       return Mage::getSingleton('exxab/catalog_layer');
    }
 
}