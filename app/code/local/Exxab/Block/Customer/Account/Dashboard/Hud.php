<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Dashboard Customer Info
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Exxab_Block_Customer_Account_Dashboard_Hud extends Mage_Core_Block_Template
{
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
    
    public function getCustomerBoughtOrders()
    {
        $orders = Mage::getModel('sales/order')
                ->getCollection()
                ->addFieldToSelect('increment_id')
                ->addFieldToFilter('customer_id',$this->getCustomer()->getId());   
        return $orders->getSize();
    }
    
    public function getSellerProducts() 
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('seller_id', $this->getCustomer()->getId())
                ->addAttributeToFilter('marketplace_state', array('neq' => Mage::helper('marketplace')->getDeletedOptionValue()));
        return $collection->getSize();
    }

    public function getSellerSoldOrders()
    {
        $orderItems = Mage::getResourceModel('sales/order_item_collection')
        ->addFieldToSelect('order_id')
        ->addFieldToFilter('seller_id', $this->getCustomer()->getId())
        ->distinct(true);
        return $orderItems->getSize();
    }

    public function getSellerPendingShipments()
    {
        $orderItems = Mage::getResourceModel('sales/order_item_collection')
        ->addFieldToSelect('order_id')
        ->addFieldToFilter('seller_id', $this->getCustomer()->getId())
        ->distinct(true);
        
        $arr_order_for_filter=array();
        foreach ($orderItems->getData() as $orderItem) {
            $arr_order_for_filter[]=$orderItem['order_id'];
        }

        $orders = Mage::getResourceModel('sales/order_collection')
        ->addFieldToSelect('*')
        ->addFieldToFilter('entity_id',array('in'=>$arr_order_for_filter))
        ->addFieldToFilter('status', 'pending_shipment');

        return $orders->getSize();
    }

    public function getCustomerShippedOrders()
    {
        $orders = Mage::getModel('sales/order')
                ->getCollection()
                ->addFieldToSelect('increment_id')
                ->addFieldToFilter('status', 'shipped')
                ->addFieldToFilter('customer_id',$this->getCustomer()->getId());   
        return $orders->getSize();
    }

    public function getSellerCredit()
    {
        $orderItems = Mage::getResourceModel('sales/order_item_collection')
        ->addFieldToSelect('order_id')
        ->addFieldToFilter('seller_id', $this->getCustomer()->getId())
        ->distinct(true);
        
        $arr_order_for_filter=array();
        foreach ($orderItems->getData() as $orderItem) {
            $arr_order_for_filter[]=$orderItem['order_id'];
        }

        $orders = Mage::getResourceModel('sales/order_collection')
        ->addFieldToSelect('*')
        ->addFieldToFilter('entity_id',array('in'=>$arr_order_for_filter))
        ->addFieldToFilter('status', 'complete');
        
        $credit = 0;
        foreach ($orders as $order) {
            $credit += Mage::helper('marketplace')->getOrderTotals($order->getId())['grandtotal'];   

        }
        return $credit;
    }
    
}