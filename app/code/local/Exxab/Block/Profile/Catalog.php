<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Multishipping checkout choose item addresses block
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Block_Profile_Catalog extends Mage_Core_Block_Template
{
    protected $_categories;
    protected $_sellerCategories;

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->_categories = Mage::helper('marketplace')->getCategoryCollection();
      
        $customerId = $this->getCustomer()->getId();
        $this->_sellerCategories = Mage::getModel('exxab/sellercategory')->getSellerCategoryCollection($customerId);  

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle(Mage::helper('exxab')->__('Catalog Information') . ' - ' . $headBlock->getDefaultTitle());
        }
    }

    public function sellerHasCategory($category) 
    {
        $sellerCategoryIds = $this->_sellerCategories->getCategoryIds();
        foreach($sellerCategoryIds as $sellerCategoryId) {
            if(in_array($category, $sellerCategoryIds)) {
                return true;
            }
        }
        return false;
    }

    public function getCategoryCollection() 
    {
        return $this->_categories;
    }
    
    public function getCategoryCount() 
    {
        return $this->_categories->getSize();
    }
 
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
    
    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/catalogPost');
    }
    
    public function getBackUrl()
    {
        return Mage::getUrl('*/*/backtoinfo');
    }

    public function getSkipUrl()
    {
        return Mage::getUrl('*/*/financial');
    }

}