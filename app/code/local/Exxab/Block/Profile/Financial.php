<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Multishipping checkout choose item addresses block
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Block_Profile_Financial extends Mage_Core_Block_Template
{
    
    protected $_bankAccount;
    protected $_paypalAccount;
    protected $_receiptMethod;

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $customerId = $this->getCustomer()->getId();
        $this->_bankAccount = Mage::getModel('exxab/sellerbank');
        $this->_paypalAccount = Mage::getModel('exxab/sellerpaypal');
        $this->_receiptMethod = Mage::getModel('customer/customer')->load($customerId)->getDefaultReceipt();

        // Init sellerbank object
        $this->_bankAccount->load($customerId);
        if (!$this->_bankAccount->getSellerId()) {
            $this->_bankAccount->setData(array());
        }
  
        // Init sellerpaypal object
        $this->_paypalAccount->load($customerId);
        if (!$this->_paypalAccount->getSellerId()) {
            $this->_paypalAccount->setData(array());
        }

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle(Mage::helper('exxab')->__('Account Financial Settings') . ' - ' . $headBlock->getDefaultTitle());
        }

        if ($postedData = Mage::getSingleton('exxab/session')->getFormData(true)) {
            $this->_bankAccount->addData($postedData);
            $this->_paypalAccount->addData($postedData);
            $this->_receiptMethod = $postedData['receipt_method'];  
        }
    }

    public function getReceiptMethod()
    {
        return $this->_receiptMethod;
    }

    public function getPaypalAccount() 
    {
        return $this->_paypalAccount;
    }

    public function getEmail() 
    {
        return $this->getPaypalAccount()->getEmail();
    }

    public function getBankAccount() 
    {
        return $this->_bankAccount;
    }

    public function getHolderName() 
    {
        return $this->getBankAccount()->getHolderName();
    }

    public function getBankName() 
    {
        return $this->getBankAccount()->getBankName();        
    }

    public function getAccountNumber() 
    {
        return $this->getBankAccount()->getAccountNumber();
    }

    public function getBranch() 
    {
        return $this->getBankAccount()->getBranch();
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
    
    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/financialPost');
    }
    
    public function getBackUrl()
    {
        return Mage::getUrl('*/*/backtocatalog');
    }

    public function getSkipUrl()
    {
        return Mage::getUrl('*/*/success');
    }
}