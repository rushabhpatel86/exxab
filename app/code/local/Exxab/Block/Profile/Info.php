<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Multishipping checkout choose item addresses block
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Block_Profile_Info extends Mage_Core_Block_Template
{
    protected $_displayName;

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $customerId = $this->getCustomer()->getId();
        $this->_displayName = Mage::getModel('customer/customer')->load($customerId)->getCompanyName();

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle(Mage::helper('exxab')->__('Seller Information') . ' - ' . $headBlock->getDefaultTitle());
        }
        
    }

    public function getDisplayName()
    {
        return $this->_displayName;
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
    
    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/infoPost');
    }
 
    public function getAddAddressUrl()
    {
        return Mage::getUrl('*/profile_address/newBusiness');
    }   
    
    public function getBackUrl()
    {
        return Mage::getUrl('*/*/backtostart');
    }
    
    public function getSkipUrl()
    {
        return Mage::getUrl('*/*/catalog');
    }

    public function isCustomerLoggedIn()
    {
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }

    public function customerHasAddresses()
    {
        return count($this->getCustomer()->getAddresses());
    }

    /* */
    public function getBusinessAddressHtmlSelect()
    {
        if ($this->isCustomerLoggedIn()) {
            $options = array();
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $options[] = array(
                    'value' => $address->getId(),
                    'label' => $address->format('oneline')
                );
            }

            $address = $this->getCustomer()->getPrimaryBusinessAddress();

            if ($address) {
                $addressId = $address->getId();
            }

            $select = $this->getLayout()->createBlock('core/html_select')
                ->setName('business_address_id')
                ->setId('business-address-select')
                ->setClass('address-select')
                ->setValue($addressId)
                ->setOptions($options);

            return $select->getHtml();
        }
        return '';
    }

    public function getAddresses() {
        return $this->getCustomer()->getAddresses();
    }
    
    public function getAddressHtml($address)
    {
        return $address->format('html');
        //return $address->toString($address->getHtmlFormat());
    }



}