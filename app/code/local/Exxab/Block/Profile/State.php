<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Multishipping checkout state
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Block_Profile_State extends Mage_Core_Block_Template
{
    public function getSteps()
    {
        return Mage::getSingleton('exxab/profile_state')->getSteps();
    }

    public function getProgress()
    {
        $state = Mage::getSingleton('exxab/profile_state');
        $progress = 0;
        if($state->getCompleteStep(Exxab_Model_Profile_State::STEP_INFO)) {
            $progress = $progress + 1;
        }
        if($state->getCompleteStep(Exxab_Model_Profile_State::STEP_CATALOG)) {
            $progress = $progress + 1;
        }
        if($state->getCompleteStep(Exxab_Model_Profile_State::STEP_FINANCIAL)) {
            $progress = $progress + 1;
        }
        return intval($progress / 3 * 100);
    }
}