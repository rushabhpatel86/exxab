<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Multishipping checkout choose item addresses block
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Block_Profile_Success extends Mage_Core_Block_Template
{
    protected $_customer;

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
        $this->_customer = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getId());
 
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle(Mage::helper('exxab')->__('Setting Up Your Account') . ' - ' . $headBlock->getDefaultTitle());
        }
    }

    protected function _getState()
    {
        return Mage::getSingleton('exxab/profile_state');
    }

    public function isStepInfoComplete()
    {
        if ($this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_INFO)) {
            return true;
        }
        return false;
    }

    public function isStepCatalogComplete()
    {
        if ($this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_CATALOG)) {
            return true;
        }
        return false;
    }

    public function isStepFinancialComplete()
    {
        if ($this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_FINANCIAL)) {
            return true;
        }
        return false;
    }

    public function isStepsComplete() 
    {
        if ( $this->isStepInfoComplete() 
          && $this->isStepCatalogComplete() 
          && $this->isStepFinancialComplete() ) {
            return true;
        }
        return false;
    }

    public function getCustomer()
    {
        return $this->_customer;
    }

    public function getDisplayName() 
    {
        return $this->getCustomer()->getCompanyName();
    }
    
    public function getBusinessAddress() 
    {
        return $this->getCustomer()->getPrimaryBusinessAddress()->format('html');
    }  

    public function getSellerCategories() 
    {
        return Mage::getModel('exxab/sellercategory')->getSellerCategoryCollection($this->_customer->getId());
    }

    public function getReceiptMethod()
    {
        return $this->_customer->getDefaultReceipt();
    }

    public function getBankAccount()
    {
        return Mage::getModel('exxab/sellerbank')->load($this->_customer->getId());
    }
    
    public function getPaypalAccount()
    {
        return Mage::getModel('exxab/sellerpaypal')->load($this->_customer->getId());
    }
    
    public function getEditSellerInfoUrl()
    {
        return $this->getUrl('*/*/info');
    }

    public function getEditSellerCatalogUrl()
    {
        return $this->getUrl('*/*/catalog');
    }

    public function getEditSellerFinancialUrl()
    {
        return $this->getUrl('*/*/financial');
    }

    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/successPost');
    }
}