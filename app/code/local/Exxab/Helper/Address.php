<?php
class Exxab_Helper_Address extends Mage_Core_Helper_Abstract
{
    public function getDefaultBusiness(Mage_Customer_Model_Customer $customer)
    {
        return $customer->getData('default_business_address_id');
    }
    public function getDefaultBusinessAddress(Mage_Customer_Model_Customer $customer)
    {
        return $customer->getPrimaryAddress($customer->getData('default_business_address_id'));
    }
}