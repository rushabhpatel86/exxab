<?php

/**
 * YouAMA.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA that is bundled with this package
 * on http://youama.com/freemodule-license.txt.
 *
 *******************************************************************************
 *                          MAGENTO EDITION USAGE NOTICE
 *******************************************************************************
 * This package designed for Magento Community edition. Developer(s) of
 * YouAMA.com does not guarantee correct work of this extension on any other
 * Magento edition except Magento Community edition. YouAMA.com does not
 * provide extension support in case of incorrect edition usage.
 *******************************************************************************
 *                                  DISCLAIMER
 *******************************************************************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 *******************************************************************************
 * @category   Youama
 * @package    Youama_Ajaxlogin
 * @copyright  Copyright (c) 2012-2014 YouAMA.com (http://www.youama.com)
 * @license    http://youama.com/freemodule-license.txt
 */

/**
 * Register user.
 * Class Youama_Ajaxlogin_Model_Ajaxregister
 * @author doveid
 */
class Exxab_Model_Ajaxregister extends Youama_Ajaxlogin_Model_Ajaxregister
{
    /**
     * Register user via Mage's API.
     */
    protected function _registerUser()
    {
        // Empty customer object
        $customer = Mage::getModel('customer/customer');

        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());

        // Set customer
        $customer->setEmail($this->_userEmail);
        $customer->setPassword($this->_userPassword);
        $customer->setFirstname($this->_userFirstName);
        $customer->setLastname($this->_userLastName);
        
        // Set customer as seller
        $approved = Mage::getStoreConfig('marketplace/status/approved');
        $customer->setSellerSubscriber(1);
        $customer->setStatus($approved);

        // Try create customer
        try {
            $customer->save();
            $customer->setConfirmation(null);
            $customer->save();

            $storeId = $customer->getSendemailStoreId();
            $customer->sendNewAccountEmail('registered', '', $storeId);
            
            Mage::getSingleton('customer/session')->loginById($customer->getId());
            
            $this->_userId = $customer->getId();
            
            $this->_result = 'success';
            
        // Error by injected HTML/JS
        } catch (Exception $ex) {
            $this->_result .= 'frontendhackerror,';
        }
    }
}