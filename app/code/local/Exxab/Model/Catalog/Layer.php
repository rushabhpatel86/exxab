<?php
 
class Exxab_Model_Catalog_Layer extends Mage_Catalog_Model_Layer
{
 
    public function prepareProductCollection($collection)
    {
        parent::prepareProductCollection($collection);
        
        $seller_id = (int)Mage::getBlockSingleton('marketplace/seller_info')->getSellerData()[entity_id];
        $collection
            ->addAttributeToFilter('seller_id', $seller_id); 
 
        return $this;
    }
 
}