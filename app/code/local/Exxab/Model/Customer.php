<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer model
 *
 * @category    Mage
 * @package     Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Model_Customer extends Mage_Customer_Model_Customer
{
    /**
     * Get customer default business address
     *
     * @return Mage_Customer_Model_Address
     */
    public function getPrimaryBusinessAddress()
    {
        return $this->getPrimaryAddress('default_business_address_id');
    }

    /**
     * Get customer default billing address
     *
     * @return Mage_Customer_Model_Address
     */
    public function getDefaultBusinessAddress()
    {
        return $this->getPrimaryBusinessAddress();
    }
    
    /**
     * Retrieve ids of default addresses
     *
     * @return array
     */
    public function getPrimaryAddressIds()
    {
        $ids = array();
        if ($this->getDefaultBilling()) {
            $ids[] = $this->getDefaultBilling();
        }
        if ($this->getDefaultShipping()) {
            $ids[] = $this->getDefaultShipping();
        }
        if ($this->getDefaultBusinessAddressId()) {
            $ids[] = $this->getDefaultBusinessAddressId();
        }
        return $ids;
    }

    /**
     * Retrieve all customer default addresses
     *
     * @return array
     */
    public function getPrimaryAddresses()
    {
        $addresses = array();
        $primaryBilling = $this->getPrimaryBillingAddress();
        if ($primaryBilling) {
            $addresses[] = $primaryBilling;
            $primaryBilling->setIsPrimaryBilling(true);
        }

        $primaryShipping = $this->getPrimaryShippingAddress();
        if ($primaryShipping) {
            if ($primaryBilling->getId() == $primaryShipping->getId()) {
                $primaryBilling->setIsPrimaryShipping(true);
            } else {
                $primaryShipping->setIsPrimaryShipping(true);
                $addresses[] = $primaryShipping;
            }
        }

        $primaryBusiness = $this->getPrimaryBusinessAddress();
        if ($primaryBusiness) {
            if ($primaryBilling->getId() == $primaryBusiness->getId()) {
                $primaryBusiness->setIsPrimaryBusiness(true);
            } else if ($primaryShipping->getId() == $primaryBusiness->getId()) {
                $primaryBusiness->setIsPrimaryBusiness(true);
            } else {
                $primaryBusiness->setIsPrimaryBusiness(true);
                $addresses[] = $primaryBusiness;
            }
        }
        return $addresses;
    }

    /**
     * Retrieve not default addresses
     *
     * @return array
     */
    public function getAdditionalAddresses()
    {
        $addresses = array();
        $primatyIds = $this->getPrimaryAddressIds();
        foreach ($this->getAddressesCollection() as $address) {
            if (!in_array($address->getId(), $primatyIds)) {
                $addresses[] = $address;
            }
        }
        return $addresses;
    }
    
    /**
     * Check if address is primary
     *
     * @param Mage_Customer_Model_Address $address
     * @return boolean
     */
    public function isAddressPrimary(Mage_Customer_Model_Address $address)
    {
        if (!$address->getId()) {
            return false;
        }
        return ($address->getId() == $this->getDefaultBilling()) || ($address->getId() == $this->getDefaultShipping()) ||
               ($address->getId() == $this->getDefaultBusiness());
    }
}