<?php
class Exxab_Model_Customer_Attribute_Backend_Business extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    public function beforeSave($object)
    {
        $helper = $this->_getHelper();
        $defaultBusiness = $helper->getDefaultBusiness($object);
        if (is_null($defaultBusiness)) {
            $object->setData('default_business_address_id', null);
        }
    }
    public function afterSave($object)
    {
        if ($defaultBusiness = $this->_getHelper()->getDefaultBusiness($object))
        {
            $addressId = false;
            /**
             * post_index set in customer save action for address
             * this is $_POST array index for address
             */
            foreach ($object->getAddresses() as $address) {
                if ($address->getPostIndex() == $defaultBusiness) {
                    $addressId = $address->getId();
                }
            }
            if ($addressId) {
                $object->setDefaultBusinessAddressId($addressId);
                $this->getAttribute()->getEntity()
                    ->saveAttribute($object, $this->getAttribute()->getAttributeCode());
            }
        }
    }

    /**
     * @return StackExchange_Storefinder_Helper_Address
     */
    protected function _getHelper()
    {
        return Mage::helper('exxab/address');
    }
}