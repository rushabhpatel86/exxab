<?php
/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */

class Exxab_Model_Observer {

    /* approve as seller */
    public function setCustomerAsSeller(Varien_Event_Observer $observer) {
        $event = $observer->getEvent();
        $customer = $event->getCustomer();
        $email = $customer->getEmail();
        if($email) {
            $approved = Mage::getStoreConfig('marketplace/status/approved');
            $customer->setSellerSubscriber(1);
            $customer->setStatus($approved);
            $customer->save();
        }
    }

    /**
     * Address after save event handler
     *
     * @param Varien_Event_Observer $observer
     */
    public function afterAddressSave($observer)
    {
        /** @var $customerAddress Mage_Customer_Model_Address */
        $address = $observer->getCustomerAddress();
        $customer = $address->getCustomer();
        
        if ($address->getId() && $address->getIsDefaultBusiness()) {
            $customer = Mage::getModel('customer/customer')
                ->load($address->getCustomerId());
            $customer->setDefaultBusinessAddressId($address->getId());
            $customer->save();
        }
    }


    /* unset all session data */
    public function unsetAll()
    {
        Mage::getSingleton('exxab/session')->unsetAll();
    }
}