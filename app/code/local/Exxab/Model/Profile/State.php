<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Multishipping checkout state model
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Model_Profile_State extends Varien_Object
{
    const STEP_AGREEMENT = 'profile_agreement';
    const STEP_START     = 'profile_start';
    const STEP_INFO      = 'profile_info';
    const STEP_CATALOG   = 'profile_catalog';
    const STEP_FINANCIAL = 'profile_financial';
    const STEP_SUCCESS   = 'profile_success';

    /**
     * Allow steps array
     *
     * @var array
     */
    protected $_steps;

    /**
     * Checkout model
     *
     * @var Mage_Checkout_Model_Type_Multishipping
     */
    protected $_checkout;

    /**
     * Init model, steps
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->_steps = array(
            self::STEP_AGREEMENT => new Varien_Object(array(
                'label' => Mage::helper('exxab')->__('Seller Services')
            )),
            self::STEP_START => new Varien_Object(array(
                'label' => Mage::helper('exxab')->__('Setting Up Your Account')
            )),
            self::STEP_INFO => new Varien_Object(array(
                'label' => Mage::helper('exxab')->__('Seller Information')
            )),
            self::STEP_CATALOG => new Varien_Object(array(
                'label' => Mage::helper('exxab')->__('Catalog Information')
            )),
            self::STEP_FINANCIAL => new Varien_Object(array(
                'label' => Mage::helper('exxab')->__('Account Financial Settings')
            )),
            self::STEP_SUCCESS => new Varien_Object(array(
                'label' => Mage::helper('exxab')->__('Setting Up Your Account')
            )),
        );

        foreach ($this->_steps as $step) {
            $step->setIsComplete(false);
        }

        $this->_profile = Mage::getSingleton('exxab/session');
        $this->_steps[$this->getActiveStep()]->setIsActive(true);
    }

    /**
     * Retrieve checkout model
     *
     * @return Mage_Checkout_Model_Type_Multishipping
     */
    public function getProfile()
    {
        return $this->_profile;
    }

    /**
     * Retrieve available checkout steps
     *
     * @return array
     */
    public function getSteps()
    {
        return $this->_steps;
    }

    /**
     * Retrieve active step code
     *
     * @return string
     */
    public function getActiveStep()
    {
        $step = $this->getProfileSession()->getProfileState();
        if (isset($this->_steps[$step])) {
            return $step;
        }
        return self::STEP_AGREEMENT;
    }

    public function setActiveStep($step)
    {
        if (isset($this->_steps[$step])) {
            $this->getProfileSession()->setProfileState($step);
        }
        else {
            $this->getProfileSession()->setProfileState(self::STEP_AGREEMENT);
        }

        // Fix active step changing
        if(!$this->_steps[$step]->getIsActive()) {
            foreach($this->getSteps() as $stepObject) {
                $stepObject->unsIsActive();
            }
            $this->_steps[$step]->setIsActive(true);
        }
        return $this;
    }

    /**
     * Mark step as completed
     *
     * @param string $step
     * @return Mage_Checkout_Model_Type_Multishipping_State
     */
    public function setCompleteStep($step)
    {
        if (isset($this->_steps[$step])) {
            $this->getProfileSession()->setStepData($step, 'is_complete', true);
        }
        return $this;
    }

    /**
     * Retrieve step complete status
     *
     * @param string $step
     * @return bool
     */
    public function getCompleteStep($step)
    {
        if (isset($this->_steps[$step])) {
            return $this->getProfileSession()->getStepData($step, 'is_complete');
        }
        return false;
    }

    /**
     * Unset complete status from step
     *
     * @param string $step
     * @return Mage_Checkout_Model_Type_Multishipping_State
     */
    public function unsCompleteStep($step)
    {
        if (isset($this->_steps[$step])) {
            $this->getProfileSession()->setStepData($step, 'is_complete', false);
        }
        return $this;
    }

    public function canSelectAddresses()
    {

    }

    public function canInputShipping()
    {

    }

    public function canSeeOverview()
    {

    }

    public function canSuccess()
    {

    }

    /**
     * Retrieve checkout session
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getProfileSession()
    {
        return Mage::getSingleton('exxab/session');
    }
}