<?php
class Exxab_Model_Resource_Sellerbank_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('exxab/sellerbank');
    }
}