<?php
 
class Exxab_Model_Resource_Sellercategory extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('exxab/sellercategory', 'entity_id');
    }
}