<?php
 
class Exxab_Model_Resource_Sellerpaypal extends Mage_Core_Model_Resource_Db_Abstract
{
    protected $_isPkAutoIncrement = false;

    protected function _construct()
    {
        $this->_init('exxab/sellerpaypal', 'seller_id');
    }
}