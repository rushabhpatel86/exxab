<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Order Item Model
 *
 * @method Mage_Sales_Model_Resource_Order_Item _getResource()
 * @method Mage_Sales_Model_Resource_Order_Item getResource()
 * @method int getOrderId()
 * @method Mage_Sales_Model_Order_Item setOrderId(int $value)
 * @method int getParentItemId()
 * @method Mage_Sales_Model_Order_Item setParentItemId(int $value)
 * @method int getQuoteItemId()
 * @method Mage_Sales_Model_Order_Item setQuoteItemId(int $value)
 * @method int getStoreId()
 * @method Mage_Sales_Model_Order_Item setStoreId(int $value)
 * @method string getCreatedAt()
 * @method Mage_Sales_Model_Order_Item setCreatedAt(string $value)
 * @method string getUpdatedAt()
 * @method Mage_Sales_Model_Order_Item setUpdatedAt(string $value)
 * @method int getProductId()
 * @method Mage_Sales_Model_Order_Item setProductId(int $value)
 * @method string getProductType()
 * @method Mage_Sales_Model_Order_Item setProductType(string $value)
 * @method float getWeight()
 * @method Mage_Sales_Model_Order_Item setWeight(float $value)
 * @method int getIsVirtual()
 * @method Mage_Sales_Model_Order_Item setIsVirtual(int $value)
 * @method string getSku()
 * @method Mage_Sales_Model_Order_Item setSku(string $value)
 * @method string getName()
 * @method Mage_Sales_Model_Order_Item setName(string $value)
 * @method string getDescription()
 * @method Mage_Sales_Model_Order_Item setDescription(string $value)
 * @method string getAppliedRuleIds()
 * @method Mage_Sales_Model_Order_Item setAppliedRuleIds(string $value)
 * @method string getAdditionalData()
 * @method Mage_Sales_Model_Order_Item setAdditionalData(string $value)
 * @method int getFreeShipping()
 * @method Mage_Sales_Model_Order_Item setFreeShipping(int $value)
 * @method int getIsQtyDecimal()
 * @method Mage_Sales_Model_Order_Item setIsQtyDecimal(int $value)
 * @method int getNoDiscount()
 * @method Mage_Sales_Model_Order_Item setNoDiscount(int $value)
 * @method float getQtyBackordered()
 * @method Mage_Sales_Model_Order_Item setQtyBackordered(float $value)
 * @method float getQtyCanceled()
 * @method Mage_Sales_Model_Order_Item setQtyCanceled(float $value)
 * @method float getQtyInvoiced()
 * @method Mage_Sales_Model_Order_Item setQtyInvoiced(float $value)
 * @method float getQtyOrdered()
 * @method Mage_Sales_Model_Order_Item setQtyOrdered(float $value)
 * @method float getQtyRefunded()
 * @method Mage_Sales_Model_Order_Item setQtyRefunded(float $value)
 * @method float getQtyShipped()
 * @method Mage_Sales_Model_Order_Item setQtyShipped(float $value)
 * @method float getBaseCost()
 * @method Mage_Sales_Model_Order_Item setBaseCost(float $value)
 * @method float getPrice()
 * @method Mage_Sales_Model_Order_Item setPrice(float $value)
 * @method float getBasePrice()
 * @method Mage_Sales_Model_Order_Item setBasePrice(float $value)
 * @method Mage_Sales_Model_Order_Item setOriginalPrice(float $value)
 * @method float getBaseOriginalPrice()
 * @method Mage_Sales_Model_Order_Item setBaseOriginalPrice(float $value)
 * @method float getTaxPercent()
 * @method Mage_Sales_Model_Order_Item setTaxPercent(float $value)
 * @method float getTaxAmount()
 * @method Mage_Sales_Model_Order_Item setTaxAmount(float $value)
 * @method float getBaseTaxAmount()
 * @method Mage_Sales_Model_Order_Item setBaseTaxAmount(float $value)
 * @method float getTaxInvoiced()
 * @method Mage_Sales_Model_Order_Item setTaxInvoiced(float $value)
 * @method float getBaseTaxInvoiced()
 * @method Mage_Sales_Model_Order_Item setBaseTaxInvoiced(float $value)
 * @method float getDiscountPercent()
 * @method Mage_Sales_Model_Order_Item setDiscountPercent(float $value)
 * @method float getDiscountAmount()
 * @method Mage_Sales_Model_Order_Item setDiscountAmount(float $value)
 * @method float getBaseDiscountAmount()
 * @method Mage_Sales_Model_Order_Item setBaseDiscountAmount(float $value)
 * @method float getDiscountInvoiced()
 * @method Mage_Sales_Model_Order_Item setDiscountInvoiced(float $value)
 * @method float getBaseDiscountInvoiced()
 * @method Mage_Sales_Model_Order_Item setBaseDiscountInvoiced(float $value)
 * @method float getAmountRefunded()
 * @method Mage_Sales_Model_Order_Item setAmountRefunded(float $value)
 * @method float getBaseAmountRefunded()
 * @method Mage_Sales_Model_Order_Item setBaseAmountRefunded(float $value)
 * @method float getRowTotal()
 * @method Mage_Sales_Model_Order_Item setRowTotal(float $value)
 * @method float getBaseRowTotal()
 * @method Mage_Sales_Model_Order_Item setBaseRowTotal(float $value)
 * @method float getRowInvoiced()
 * @method Mage_Sales_Model_Order_Item setRowInvoiced(float $value)
 * @method float getBaseRowInvoiced()
 * @method Mage_Sales_Model_Order_Item setBaseRowInvoiced(float $value)
 * @method float getRowWeight()
 * @method Mage_Sales_Model_Order_Item setRowWeight(float $value)
 * @method int getGiftMessageId()
 * @method Mage_Sales_Model_Order_Item setGiftMessageId(int $value)
 * @method int getGiftMessageAvailable()
 * @method Mage_Sales_Model_Order_Item setGiftMessageAvailable(int $value)
 * @method float getBaseTaxBeforeDiscount()
 * @method Mage_Sales_Model_Order_Item setBaseTaxBeforeDiscount(float $value)
 * @method float getTaxBeforeDiscount()
 * @method Mage_Sales_Model_Order_Item setTaxBeforeDiscount(float $value)
 * @method string getExtOrderItemId()
 * @method Mage_Sales_Model_Order_Item setExtOrderItemId(string $value)
 * @method string getWeeeTaxApplied()
 * @method Mage_Sales_Model_Order_Item setWeeeTaxApplied(string $value)
 * @method float getWeeeTaxAppliedAmount()
 * @method Mage_Sales_Model_Order_Item setWeeeTaxAppliedAmount(float $value)
 * @method float getWeeeTaxAppliedRowAmount()
 * @method Mage_Sales_Model_Order_Item setWeeeTaxAppliedRowAmount(float $value)
 * @method float getBaseWeeeTaxAppliedAmount()
 * @method Mage_Sales_Model_Order_Item setBaseWeeeTaxAppliedAmount(float $value)
 * @method float getBaseWeeeTaxAppliedRowAmount()
 * @method Mage_Sales_Model_Order_Item setBaseWeeeTaxAppliedRowAmount(float $value)
 * @method float getWeeeTaxDisposition()
 * @method Mage_Sales_Model_Order_Item setWeeeTaxDisposition(float $value)
 * @method float getWeeeTaxRowDisposition()
 * @method Mage_Sales_Model_Order_Item setWeeeTaxRowDisposition(float $value)
 * @method float getBaseWeeeTaxDisposition()
 * @method Mage_Sales_Model_Order_Item setBaseWeeeTaxDisposition(float $value)
 * @method float getBaseWeeeTaxRowDisposition()
 * @method Mage_Sales_Model_Order_Item setBaseWeeeTaxRowDisposition(float $value)
 * @method int getLockedDoInvoice()
 * @method Mage_Sales_Model_Order_Item setLockedDoInvoice(int $value)
 * @method int getLockedDoShip()
 * @method Mage_Sales_Model_Order_Item setLockedDoShip(int $value)
 * @method float getPriceInclTax()
 * @method Mage_Sales_Model_Order_Item setPriceInclTax(float $value)
 * @method float getBasePriceInclTax()
 * @method Mage_Sales_Model_Order_Item setBasePriceInclTax(float $value)
 * @method float getRowTotalInclTax()
 * @method Mage_Sales_Model_Order_Item setRowTotalInclTax(float $value)
 * @method float getBaseRowTotalInclTax()
 * @method Mage_Sales_Model_Order_Item setBaseRowTotalInclTax(float $value)
 * @method float getHiddenTaxAmount()
 * @method Mage_Sales_Model_Order_Item setHiddenTaxAmount(float $value)
 * @method float getBaseHiddenTaxAmount()
 * @method Mage_Sales_Model_Order_Item setBaseHiddenTaxAmount(float $value)
 * @method float getHiddenTaxInvoiced()
 * @method Mage_Sales_Model_Order_Item setHiddenTaxInvoiced(float $value)
 * @method float getBaseHiddenTaxInvoiced()
 * @method Mage_Sales_Model_Order_Item setBaseHiddenTaxInvoiced(float $value)
 * @method float getHiddenTaxRefunded()
 * @method Mage_Sales_Model_Order_Item setHiddenTaxRefunded(float $value)
 * @method float getBaseHiddenTaxRefunded()
 * @method Mage_Sales_Model_Order_Item setBaseHiddenTaxRefunded(float $value)
 * @method int getIsNominal()
 * @method Mage_Sales_Model_Order_Item setIsNominal(int $value)
 * @method float getTaxCanceled()
 * @method Mage_Sales_Model_Order_Item setTaxCanceled(float $value)
 * @method float getHiddenTaxCanceled()
 * @method Mage_Sales_Model_Order_Item setHiddenTaxCanceled(float $value)
 * @method float getTaxRefunded()
 * @method Mage_Sales_Model_Order_Item setTaxRefunded(float $value)
 * @method float getBaseTaxRefunded()
 * @method Mage_Sales_Model_Order_Item setBaseTaxRefunded(float $value)
 * @method float getDiscountRefunded()
 * @method Mage_Sales_Model_Order_Item setDiscountRefunded(float $value)
 * @method float getBaseDiscountRefunded()
 * @method Mage_Sales_Model_Order_Item setBaseDiscountRefunded(float $value)
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Model_Sales_Order_Item extends Mage_Sales_Model_Order_Item
{
    public function getSellerName()
    {   
        if ($this->getSellerId()) {
            $sellerName = Mage::helper('exxab')->getSellerName($this->getSellerId());
        }
        return $sellerName;
    }
}