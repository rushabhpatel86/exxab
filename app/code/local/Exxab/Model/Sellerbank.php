<?php
 
class Exxab_Model_Sellerbank extends Mage_Core_Model_Abstract
{
    /**
     * List of errors
     *
     * @var array
     */
    protected $_errors = array();

    protected function _construct()
    {
        $this->_init('exxab/sellerbank');
    }

    /**
     * Validate address attribute values
     *
     * @return array | bool
     */
    public function validate()
    {
        $this->_resetErrors();

        if (!Zend_Validate::is($this->getHolderName(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the holder name.'));
        }

        if (!Zend_Validate::is($this->getBankName(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the bank name.'));
        }

        if (!Zend_Validate::is($this->getAccountNumber(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the account number.'));
        }

        if (!Zend_Validate::is($this->getBranch(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the branch.'));
        }

        $errors = $this->_getErrors();

        $this->_resetErrors();

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    /**
     * Add error
     *
     * @param $error
     * @return Mage_Customer_Model_Address_Abstract
     */
    public function addError($error)
    {
        $this->_errors[] = $error;
        return $this;
    }

    /**
     * Retreive errors
     *
     * @return array
     */
    protected function _getErrors()
    {
        return $this->_errors;
    }

    /**
     * Reset errors array
     *
     * @return Mage_Customer_Model_Address_Abstract
     */
    protected function _resetErrors()
    {
        $this->_errors = array();
        return $this;
    }

}