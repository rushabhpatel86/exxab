<?php
 
class Exxab_Model_Sellercategory extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('exxab/sellercategory');
    }

    public function getSellerCategoryCollection($sellerId) 
    {
        return $this->getCollection()->addFieldToFilter('seller_id', $sellerId );
    }
    
}