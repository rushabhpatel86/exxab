<?php
 
class Exxab_Model_Sellerpaypal extends Mage_Core_Model_Abstract
{
    /**
     * List of errors
     *
     * @var array
     */
    protected $_errors = array();

    protected function _construct()
    {
        $this->_init('exxab/sellerpaypal');
    }

    /**
     * Validate address attribute values
     *
     * @return array | bool
     */
    public function validate()
    {
        $this->_resetErrors();

        if (!Zend_Validate::is($this->getEmail(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the paypal email.'));
        }

        $errors = $this->_getErrors();

        $this->_resetErrors();

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    /**
     * Add error
     *
     * @param $error
     * @return Mage_Customer_Model_Address_Abstract
     */
    public function addError($error)
    {
        $this->_errors[] = $error;
        return $this;
    }

    /**
     * Retreive errors
     *
     * @return array
     */
    protected function _getErrors()
    {
        return $this->_errors;
    }

    /**
     * Reset errors array
     *
     * @return Mage_Customer_Model_Address_Abstract
     */
    protected function _resetErrors()
    {
        $this->_errors = array();
        return $this;
    }

}