<?php
/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */

class Exxab_Adminhtml_Cs_Order_ItemController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init action breadcrumbs and active menu
     */
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('exxab_cs/orders')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Manage Orders'), Mage::helper('adminhtml')->__('Edit Order Item'));
        return $this;
    }

    /**
     * index action 
     */
    public function indexAction() { 
        $orderId = (int) $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order');

        if ($orderId) {
            $order->load($orderId);
        }

        if ($order->getId()) {
            $itemId = (int) $this->getRequest()->getParam('item_id');
            $item = Mage::getModel('sales/order_item');
            
            if ($itemId) {
                $item->load($itemId);
            }
            
            if ($item->getId()) {
                Mage::register('current_order', $order);
                Mage::register('current_order_item', $item);
                $this->loadLayout();
                $this->renderLayout();
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exxab')->__('Order item does not exist'));
                //$this->_redirect('*/adminhtml_cs_order/');
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exxab')->__('Order does not exist'));
            $this->_redirect('*/adminhtml_cs_order/');
        }
        
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('exxab_cs/orders');
    }
    
}