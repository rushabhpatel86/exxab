
OrderController.php
PHP script text
<?php

/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */
class Exxab_Adminhtml_Cs_OrderController extends Mage_Adminhtml_Controller_Action {

    /**
     * Init action breadcrumbs and active menu
     */
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('exxab_cs/orders')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Manage Orders'), Mage::helper('adminhtml')->__('Edit Order Item'));
        return $this;
    }

    /**
     * index action
     */
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * grid action for AJAX request
     */
    public function pendingBuyerGridAction() {
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_pendingbuyer')->toHtml()
        );
    }

    /**
     * grid action for AJAX request
     */
    public function pendingSellerGridAction() {
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_pendingseller')->toHtml()
        );
    }

    public function holdGridAction() {
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_hold')->toHtml()
        );
    }

    public function exportPendingBuyerCsvAction() {
        $fileName = 'pending_buyer_orders.csv';
        $grid = $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_pendingbuyer');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportPendingBuyerExcelAction() {
        $fileName = 'pending_buyer_orders.xml';
        $grid = $this->getLayout()->createBlock('exxab/adminhtml_cs_order_tab_pendingbuyer');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Order status edit action
     */
    public function viewAction() {
        $orderId = (int) $this->getRequest()->getParam('id');
        $order = Mage::getModel('sales/order');

        if ($orderId) {
            $order->load($orderId);
        }

        if ($order->getId()) {
            Mage::register('current_order', $order);
            $this->loadLayout();

            //$this->_addContent($this->getLayout()->createBlock('exxab/adminhtml_cs_order_view'))
            //     ->_addLeft($this->getLayout()->createBlock('exxab/adminhtml_cs_order_view_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exxab')->__('Order does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('exxab_cs/orders');
    }

}
