<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Multishipping checkout address matipulation controller
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Exxab_Profile_AddressController extends Mage_Core_Controller_Front_Action
{
    /**
     * Retrieve multishipping checkout model
     *
     * @return Mage_Checkout_Model_Type_Multishipping
     */
    protected function _getProfile()
    {
        return Mage::getSingleton('exxab/profile');
    }

    /**
     * Retrieve checkout state model
     *
     * @return Mage_Checkot_Model_Type_Multishipping_State
     */
    protected function _getState()
    {
        return Mage::getSingleton('exxab/profile_state');
    }


    /**
     * Create New Business address Form
     */
    public function newBusinessAction()
    {
        if (!$this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_AGREEMENT)) {
            $this->_redirect('*/profile/agreement');
            return $this;
        }

        $this->_getState()->setActiveStep(Exxab_Model_Profile_State::STEP_INFO);

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        if ($addressForm = $this->getLayout()->getBlock('customer_address_edit')) {
            $addressForm->setTitle(Mage::helper('exxab')->__('Create Business Address'))
                ->setSuccessUrl(Mage::getUrl('*/*/businessSaved'))
                ->setErrorUrl(Mage::getUrl('*/*/*'));

            if ($headBlock = $this->getLayout()->getBlock('head')) {
                $headBlock->setTitle($addressForm->getTitle() . ' - ' . $headBlock->getDefaultTitle());
            }

            if ($this->_getProfile()->getCustomerDefaultBusinessAddress()) {
                $addressForm->setBackUrl(Mage::getUrl('*/profile/info'));
            }
            else {
                $addressForm->setBackUrl(Mage::getUrl('*/profile/start/'));
            }
        }
        $this->renderLayout();
    }

    public function BusinessSavedAction()
    {
        /**
         * if we create first address we need reset emd init checkout
         */
        if (count($this->_getProfile()->getCustomer()->getAddresses()) == 1) {
            //$this->_getProfile()->reset();
        }
        $this->_redirect('*/profile/info');
    }
 

}