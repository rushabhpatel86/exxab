<?php

/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */
class Exxab_ProfileController extends Mage_Core_Controller_Front_Action {

    public function preDispatch() {
        parent::preDispatch();

        $action = strtolower($this->getRequest()->getActionName());

        /**
         * Catch index action call to set some flags before checkout/type_multishipping model initialization
         */
        if ($action == 'index') {
            $this->_getProfileSession()->setProfileState(
                    Exxab_Model_Session::PROFILE_STATE_BEGIN
            );
        }

        $this->_validateCustomerLogin();
    }

    /**
     * Retrieve checkout model
     *
     * @return Mage_Checkout_Model_Type_Multishipping
     */
    protected function _getProfile() {
        return Mage::getSingleton('exxab/profile');
    }

    /**
     * Retrieve checkout state model
     *
     * @return Mage_Checkout_Model_Type_Multishipping_State
     */
    protected function _getState() {
        return Mage::getSingleton('exxab/profile_state');
    }

    /**
     * Retrieve checkout session
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getProfileSession() {
        return Mage::getSingleton('exxab/session');
    }

    /**
     *    Seller profile agreement
     * */
    public function agreementAction() {

        $this->_getState()->unsCompleteStep(
                Exxab_Model_Profile_State::STEP_AGREEMENT
        );

        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_AGREEMENT
        );

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('exxab/session');
        $this->renderLayout();
    }

    public function agreementPostAction() {

        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/');
        }

        if ($this->getRequest()->isPost()) {

            $errors = array();

            try {

                if (!Zend_Validate::is($this->getRequest()->getPost('legal_name'), 'NotEmpty')) {
                    $errors[] = $this->__('Please enter the legal name.');
                }

                $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
                if ($requiredAgreements) {
                    $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                    $diff = array_diff($requiredAgreements, $postedAgreements);
                    if ($diff) {
                        $errors[] = $this->__('Please agree to all the terms and conditions before you proceed.');
                    }
                }

                if (count($errors) === 0) {
                    $customer = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getId());
                    $customer->setLegalName($this->getRequest()->getPost('legal_name'));
                    $customer->save();

                    $this->_getState()->setActiveStep(
                            Exxab_Model_Profile_State::STEP_START
                    );
                    $this->_getState()->setCompleteStep(
                            Exxab_Model_Profile_State::STEP_AGREEMENT
                    );
                    $this->_redirect('*/*/start');
                } else {
                    $this->_getProfileSession()->setFormData($this->getRequest()->getPost());
                    foreach ($errors as $errorMessage) {
                        $this->_getProfileSession()->addError($errorMessage);
                    }

                    return $this->_redirectError(Mage::getUrl('*/*/agreement'));
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getProfileSession()->addError($e->getMessage());
                $this->_redirect('*/*/agreement');
            }
        }
    }

    public function startAction() {

        if (!$this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_AGREEMENT)) {
            $this->_redirect('*/*/agreement');
            return $this;
        }

        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_START
        );

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('exxab/session');
        $this->renderLayout();
    }

    public function startPostAction() {
        try {
            $this->_getState()->setActiveStep(
                    Exxab_Model_Profile_State::STEP_INFO
            );
            $this->_getState()->setCompleteStep(
                    Exxab_Model_Profile_State::STEP_START
            );
            $this->_redirect('*/*/info');
        } catch (Mage_Core_Exception $e) {
            $this->_getProfileSession()->addError($e->getMessage());
            $this->_redirect('*/*/start');
        }
    }

    public function backToAgreementAction() {
        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_AGREEMENT
        );
        $this->_getState()->unsCompleteStep(
                Exxab_Model_Profile_State::STEP_START
        );
        $this->_redirect('*/*/agreement');
    }

    public function InfoAction() {

        if (!$this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_AGREEMENT)) {
            $this->_redirect('*/*/agreement');
            return $this;
        }

        // If customer do not have addresses
        if (!$this->_getProfile()->getCustomerDefaultBusinessAddress()) {
            $this->_redirect('*/profile_address/newBusiness');
            return;
        }

        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_INFO
        );

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('exxab/session');
        $this->renderLayout();
    }

    public function InfoPostAction() {

        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/');
        }

        $errors = array();

        if ($this->getRequest()->isPost()) {

            try {
                $postData = $this->getRequest()->getPost();

                if (!Zend_Validate::is($postData['display_name'], 'NotEmpty')) {
                    $errors[] = $this->__('Please enter the display name.');
                }

                if (count($errors) === 0) {
                    $customer = Mage::getSingleton('customer/session')->getCustomer();

                    $addressId = isset($postData['business_address_id']) ? $postData['business_address_id'] : false;
                    if ($addressId) {
                        $address = $customer->getAddressById($addressId);
                        if ($address->getId()) {
                            $address->setIsDefaultBusiness(true)->save();
                        }
                    }

                    $customer->setCompanyName($postData['display_name'])->save();

                    $this->_getState()->setActiveStep(
                            Exxab_Model_Profile_State::STEP_CATALOG
                    );
                    $this->_getState()->setCompleteStep(
                            Exxab_Model_Profile_State::STEP_INFO
                    );
                    $this->_redirect('*/*/catalog');
                } else {
                    $this->_getProfileSession()->setFormData($this->getRequest()->getPost());
                    foreach ($errors as $errorMessage) {
                        $this->_getProfileSession()->addError($errorMessage);
                    }
                    return $this->_redirectError(Mage::getUrl('*/*/info'));
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getProfileSession()->addError($e->getMessage());
                $this->_redirect('*/*/info');
            }
        }
    }

    public function backToStartAction() {
        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_START
        );
        $this->_getState()->unsCompleteStep(
                Exxab_Model_Profile_State::STEP_INFO
        );
        $this->_redirect('*/*/start');
    }

    public function catalogAction() {

        if (!$this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_AGREEMENT)) {
            $this->_redirect('*/*/agreement');
            return $this;
        }

        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_CATALOG
        );

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('exxab/session');
        $this->renderLayout();
    }

    public function catalogPostAction() {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/');
        }

        // Save data
        if ($this->getRequest()->isPost()) {

            $categoryIds = Mage::helper('marketplace')->getCategoryCollection()->getAllIds();
            $selectedCategoryIds = array_intersect($categoryIds, $this->getRequest()->getPost('category'));

            $errors = array();

            if (count($selectedCategoryIds) === 0) {
                $errors[] = $this->__('Please select at least one category.');
            }

            try {
                if (count($errors) === 0) {

                    $sellerId = Mage::getSingleton('customer/session')->getId();

                    $sellerCategoryCollection = Mage::getModel('exxab/sellercategory')
                            ->getSellerCategoryCollection($sellerId);

                    // delete unselected categories if exist
                    foreach ($sellerCategoryCollection as $sellerCategory) {
                        if (!in_array($sellerCategory->getCategoryId(), $selectedCategoryIds)) {
                            $sellerCategory->delete();
                        }
                    }

                    // add selected categories
                    $sellerCategoryIds = $sellerCategoryCollection->getCategoryIds();

                    foreach ($selectedCategoryIds as $selectedCategoryId) {
                        if (!in_array($selectedCategoryId, $sellerCategoryIds)) {
                            Mage::getModel('exxab/sellercategory')
                                    ->setSellerId($sellerId)
                                    ->setCategoryId($selectedCategoryId)
                                    ->save();
                        }
                    }

                    $this->_getState()->setActiveStep(
                            Exxab_Model_Profile_State::STEP_FINANCIAL
                    );
                    $this->_getState()->setCompleteStep(
                            Exxab_Model_Profile_State::STEP_CATALOG
                    );
                    $this->_redirect('*/*/financial');
                } else {

                    foreach ($errors as $errorMessage) {
                        $this->_getProfileSession()->addError($errorMessage);
                    }

                    return $this->_redirectError(Mage::getUrl('*/*/catalog'));
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getProfileSession()->addError($e->getMessage());
                $this->_redirect('*/*/catalog');
            }
        }
    }

    public function backToInfoAction() {
        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_INFO
        );
        $this->_getState()->unsCompleteStep(
                Exxab_Model_Profile_State::STEP_CATALOG
        );
        $this->_redirect('*/*/info');
    }

    public function financialAction() {

        if (!$this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_AGREEMENT)) {
            $this->_redirect('*/*/agreement');
            return $this;
        }

        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_FINANCIAL
        );

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('exxab/session');
        $this->renderLayout();
    }

    public function financialPostAction() {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/');
        }

        // Save data
        if ($this->getRequest()->isPost()) {

            $errors = array();

            try {
                $receiptMethod = $this->getRequest()->getPost('receipt_method');

                if (isset($receiptMethod) && $receiptMethod == 'bank') {
                    $methodDetails = Mage::getModel('exxab/sellerbank');
                } elseif (isset($receiptMethod) && $receiptMethod == 'paypal') {
                    $methodDetails = Mage::getModel('exxab/sellerpaypal');
                } else {
                    $errors[] = $this->__('Please select a receipt method.');
                }

                if (isset($methodDetails)) {
                    $customer = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getId());
                    $methodDetails->setSellerId($customer->getId());
                    $methodDetails->addData($this->getRequest()->getPost());

                    $methodErrors = $methodDetails->validate();
                    if ($methodErrors !== true) {
                        $errors = array_merge($errors, $methodErrors);
                    }

                    $customer->setDefaultReceipt($receiptMethod);
                }

                if (count($errors) === 0) {
                    $methodDetails->save();
                    $customer->save();

                    $this->_getState()->setActiveStep(
                            Exxab_Model_Profile_State::STEP_SUCCESS
                    );
                    $this->_getState()->setCompleteStep(
                            Exxab_Model_Profile_State::STEP_FINANCIAL
                    );
                    $this->_redirect('*/*/success');
                    return;
                } else {
                    $this->_getProfileSession()->setFormData($this->getRequest()->getPost());
                    foreach ($errors as $errorMessage) {
                        $this->_getProfileSession()->addError($errorMessage);
                    }
                }

                return $this->_redirectError(Mage::getUrl('*/*/financial'));
            } catch (Mage_Core_Exception $e) {
                $this->_getProfileSession()->addError($e->getMessage());
                $this->_redirect('*/*/financial');
            }
        }
    }

    public function backToCatalogAction() {
        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_CATALOG
        );
        $this->_getState()->unsCompleteStep(
                Exxab_Model_Profile_State::STEP_FINANCIAL
        );
        $this->_redirect('*/*/catalog');
    }

    public function successAction() {

        if (!$this->_getState()->getCompleteStep(Exxab_Model_Profile_State::STEP_FINANCIAL)) {
            $this->_redirect('*/*/agreement');
            return $this;
        }

        $this->_getState()->setActiveStep(
                Exxab_Model_Profile_State::STEP_SUCCESS
        );

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('exxab/session');
        $this->renderLayout();
    }

    public function indexAction() {
        $this->_redirect('*/*/agreement');
    }

    /**
     *    validate Customer Login and redirect previous page
     * */
    protected function _validateCustomerLogin() {
        $session = Mage::getSingleton('customer/session');
        if (!$session->isLoggedIn()) {
            $session->setAfterAuthUrl(Mage::helper('core/url')->getCurrentUrl());
            $session->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
            $this->_redirect('customer/account/login/');
            return $this;
        } elseif (!Mage::helper('marketplace')->isMarketplaceActiveSellar()) {
            $this->_redirect('customer/account/');
        }
    }

}
