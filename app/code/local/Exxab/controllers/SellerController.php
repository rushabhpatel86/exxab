<?php
/**
 * Cybage Marketplace Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Marketplace Plugin
 * @package    Cybage_Marketplace
 * @copyright  Copyright (c) 2014 Cybage Software Pvt. Ltd., India
 *             http://www.cybage.com/pages/centers-of-excellence/ecommerce/ecommerce.aspx
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Cybage Software Pvt. Ltd. <Support_Magento@cybage.com>
 */


require_once 'Cybage/Marketplace/controllers/SellerController.php';

class Exxab_SellerController extends Cybage_Marketplace_SellerController
{
    /*
    * display seller info
    * @created at 08-Oct-2013
    * @modified at 09-Oct-2013
    * @author Srinidhi Damle <srinidhid@cybage.com>
    */
    public function sellerinfoAction()
    {
        Mage::dispatchEvent(
            'catalog_controller_category_init_before',
            array(
                'controller_action' => $this
            )
        );
        
        $rootCategoryId = (int) Mage::app()->getStore()->getRootCategoryId();
        if (!$rootCategoryId) {
            $this->_forward('noRoute');
            return;
        }
 
        $rootCategory = Mage::getModel('catalog/category')
            ->load($rootCategoryId)
 
            // TODO: Fetch from config
            ->setName($this->__('Seller'))
            ->setMetaTitle($this->__('Seller'))
            ->setMetaDescription($this->__('Seller'))
            ->setMetaKeywords($this->__('Seller'));
 
        Mage::register('current_category', $rootCategory);
 
        Mage::getSingleton('catalog/session')
            ->setLastVisitedCategoryId($rootCategory->getId());

        try {
            Mage::dispatchEvent('catalog_controller_category_init_after',
                array(
                    'category' => $rootCategory,
                    'controller_action' => $this
                )
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return;
        }
 
        // Observer can change category
        if (!$rootCategory->getId()){
            $this->_forward('noRoute');
            return;
        }
 
        $this->loadLayout();
 
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('checkout/session');
 
        $this->renderLayout();
    
        
    }
    public function ratingAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}