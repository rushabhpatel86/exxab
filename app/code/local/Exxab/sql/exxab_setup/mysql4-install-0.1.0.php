<?php
/**
 * Exxab Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: Support_Magento@cybage.com.  We will send you a copy of the source file.
 *
 * @category   Exxab Plugin
 * @package    Exxab
 * @copyright  Copyright (c) 2015 Exxab.com, Jordan
 *             http://www.exxab.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Exxab.com <magento@exxab.com>
 */

$this->startSetup();

$this->addAttribute('customer', 'default_business_address_id', array(
    'type'               => 'int',
    'label'              => 'Default Business Address',
    'input'              => 'text',
    'backend'            => 'exxab/customer_attribute_backend_business',
    'required'           => false,
    'sort_order'         => 82,
    'visible'            => false,
    )
);

$this->addAttribute('customer', 'default_receipt', array(
    "type"     => "varchar",
    "label"    => "Default Receipt Method",
    "input"    => "text",
    "visible"  => false,
    "required" => false,
    )
);

$this->addAttribute('customer', 'legal_name', array(
    "type"     => "varchar",
    "label"    => "Seller Legal Name",
    "input"    => "text",
    "visible"  => false,
    "required" => false,
    )
);

$this->addAttribute('catalog_product', 'pickup_address', array(
    'label'    => 'Pickup Location',
    'type'     => 'int',
    'required' => 0,
    'visible'  => true,
    'input'    => 'select',
    'source'   => 'exxab/source_pickupaddress',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    )
);

$this->addAttribute('catalog_category', 'attribute_set', array(
    'group'            => 'General Information',
    'type'             => 'varchar',
    'label'            => 'Attribute Set',
    'input'            => 'select',
    'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'          => true,
    'required'         => false,
    'user_defined'     => false,
    'visible_on_front' => 1,
    'source'           => 'exxab/source_attributeset',
    )
);

$this->run("
    CREATE TABLE `{$this->getTable('exxab/sellerbank')}` (
        `seller_id` int(10) unsigned NOT NULL,
        `holder_name` varchar(255) NOT NULL,
        `bank_name` varchar(255) NOT NULL,
        `account_number` varchar(255) NOT NULL,
        `branch` varchar(255) NOT NULL, 
        `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`seller_id`),
        CONSTRAINT `FK_seller_bank_seller_id` FOREIGN KEY (`seller_id`) REFERENCES {$this->getTable('customer_entity')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE = InnoDB; 

    CREATE TABLE `{$this->getTable('exxab/sellerpaypal')}` (
        `seller_id` int(10) unsigned NOT NULL,
        `email` varchar(255) NOT NULL,
        `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`seller_id`),
        CONSTRAINT `FK_seller_paypal_seller_id` FOREIGN KEY (`seller_id`) REFERENCES {$this->getTable('customer_entity')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE = InnoDB;   

    CREATE TABLE `{$this->getTable('exxab/sellercategory')}` (
        `entity_id` int(10) unsigned NOT NULL auto_increment,
        `seller_id` int(10) unsigned NOT NULL,
        `category_id` int(10) NOT NULL,
        `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`entity_id`),
        CONSTRAINT `FK_seller_category_seller_id` FOREIGN KEY (`seller_id`) REFERENCES {$this->getTable('customer_entity')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT `FK_seller_category_category_id` FOREIGN KEY (`category_id`) REFERENCES {$this->getTable('catalog_category_entity')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE = InnoDB;
");

$this->endSetup();