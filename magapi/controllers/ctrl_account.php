<?php

/**
 * @author melhem
 * @copyright 2014
 */
class account
{
  var $msg="";
  var $api_token = "123456";
  var $account_data;
  var $request_status="1";

  public function __construct()
  {

    $melhem = new melhem();
    //$this->getApiTpkenId();
    $token = $melhem->getSecureParams("token_id");
    $action = $melhem->getSecureParams("action");
    if(!$token || $token!= $this->api_token){
      $this->msg = "Unauthorized";
      $this->request_status="401";
      $this->account_data['msg']=$this->msg;
      $this->account_data['status'] = $this->request_status;
      $this->viewJsonObject();
    }
    if($action) $action.="Account";
    if(method_exists($this,"$action"))
    {
      $this->$action();
    }else{

      $this->msg = "Bad Request";
      $this->request_status="400";
      $this->account_data['msg']=$this->msg;
      $this->account_data['status'] = $this->request_status;
      $this->viewJsonObject();

    }
  }
  public function getApiTpkenId()
  {
    $melhem = new melhem();
    $token = $melhem->getSecureParams("device_id",'abcdjh156a');
    $token = $token.$this->api_token;
    $token = md5($token);
    $this->api_token = $token;
  }
  public function viewJsonObject()
  {
    $melhem = new melhem();
    $jsonObject = $melhem->getJsonData($this->account_data);
    echo $jsonObject;
    exit(0);
  }
  public function newAccount()
  {
    $melhem = new melhem();
    $this->msg = "OK";
    $this->request_status="200";
    $this->account_data['msg']=$this->msg;
    $this->account_data['status'] = $this->request_status;
    $this->account_data['account'] ="";
    $this->viewJsonObject();
  }
  public function authenticationAccount()
  {
    $this->msg = "OK";
    $this->request_status="200";
    $this->account_data['msg']=$this->msg;
    $this->account_data['status'] = $this->request_status;
    $this->account_data['account'] ="";
    $this->viewJsonObject();
  }
  public function getProductsCategory()
  {

    $melhem = new melhem();
    $cat_id = $melhem->getSecureParams("cat_id");
    $page = $melhem->getSecureParams("page",'1');
    /*$client = new SoapClient(BASEAPIURL);

    $session = $client->login(APIUSER, APIKEY);
    $result = $client->call($session, 'catalog_category.assignedProducts', $cat_id);
    $client->endSession($session);

    $json = json_encode($result);
    $array = json_decode($json,TRUE);*/
    $apiUrl = 'http://yamalco.com/mag/api/rest/';
    $consumerKey = 'e7be6cea2e060a79139461f38cccb2c5';
    $consumerSecret = 'b0b57175f1b266f4c9455f253fdb49b4';
    $auth = 'bf411cf1d24e2933e75397d9ca18ed22';
    $secret = '0c04b10b1da47c86a3dec80b88d1450f';
    $state = 2;
    /*session_destroy();
    exit(0);*/



    try {
        $authType = ($state == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;
        $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
        $oauthClient->enableDebug();
    	$oauthClient->setToken($auth, $secret);
            //$resourceUrl = "$apiUrl/$action/?limit=100";
            $resourceUrl = "$apiUrl/products/?cat_id=$cat_id&page=$page";
            $oauthClient->fetch($resourceUrl, array(), 'GET', array('Content-Type' => 'application/json'));
            $productsList = json_decode($oauthClient->getLastResponse());
            $this->msg = "OK";
            $this->request_status="200";
            $this->account_data['msg']=$this->msg;
            $this->account_data['status'] = $this->request_status;
            $this->account_data['products'] =$productsList;
            $this->viewJsonObject();
    } catch (OAuthException $e) {

        print_r($e->lastResponse);
    }

  }
}
?>
