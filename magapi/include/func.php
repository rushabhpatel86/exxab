<?php

/**
 * @author melhem
 * @copyright 2014
 */

class melhem
{
  public function getSecureParams($var,$default=0)
  {
    $data = 0;
    $flag_par = 0;

    if(isset($_POST[$var]))
    {
      $data = strip_tags($_POST[$var]);
      $flag_par=1;
    }else if(isset($_GET[$var]))
    {
      $data = strip_tags($_GET[$var]);
      $flag_par=1;
    }
    if($flag_par)
    {

      $default = $data;
    }
    $default = mysql_escape_string($default);
    return $default;
  }

  public function getParams($var,$default=0)
  {
    if(isset($_POST[$var]))
    {
      $default = $_POST[$var];
    }else if(isset($_GET[$var]))
    {
      $default = $_POST[$var];
    }
    return $default;
  }
  public function getSecureParamsPut($var,$default=0)
  {
    $post_string = file_get_contents("php://input");
    if($post_string)
    {
     parse_str(file_get_contents("php://input"),$post_vars);
     if(isset($post_vars['$var']))
     {
      $default = $post_vars['$var'];
      $default = strip_tags($default);
      $default = mysql_escape_string($default);
     }
    }
    return $default;
  }
  public function getModule($module_name)
  {

    $dirname = dirname(dirname(__FILE__));
    $file_name = $dirname."/modules/$module_name/$module_name".".php";;
    $controller_name = $dirname."/modules/$module_name/$module_name".".php";;
    if(!file_exists($file_name))
    {
      $module_name = "modules/mod_error/mod_error.php";
    }else{
        $module_name = "modules/$module_name/$module_name".".php";
    }

    include($module_name);
  }

  public function getModule2()
  {
    $module_name ="mod_".$this->getSecureParams("type",0);

    $dirname = dirname(dirname(__FILE__));
    $file_name = $dirname."/modules/$module_name/$module_name".".php";;
    if(!file_exists($file_name))
    {
      $module_name = "modules/mod_error/mod_error.php";
    }else{
        $module_name = "modules/$module_name/$module_name".".php";
    }
    include($module_name);
  }
  public function getControllerHandler($controller_name="")
  {

    if(!$controller_name) $controller_name ="ctrl_".$this->getSecureParams("type");
    else $controller_name = "ctrl_".$controller_name;
    $dirname = dirname(dirname(__FILE__));
    $file_name = $dirname."/controllers/$controller_name".".php";
    if(!file_exists($file_name))
    {
      $controller_name = "controllers/ctrl_error.php";
    }else{
        $controller_name = "controllers/$controller_name".".php";

    }
    include($controller_name);
  }
  public function curPageURL()
  {
   $pageURL = 'http';
   if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
   $pageURL .= "://";
   if ($_SERVER["SERVER_PORT"] != "80") {
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
   } else {
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
   }
   return $pageURL;
  }

  public function getJsonData($dataArray)
  {
    $josnData = json_encode($dataArray);
    return $josnData;
  }
  public function newClassObject()
  {
     $newObject = null;
     $className = $this->getSecureParams("type");
     if(class_exists($className))
     {
      $newObject = new $className();
     }

     return $newObject;
  }
  public function sendNotification($msg,$registrationIds,$device_type)
  {
    $notification_msgs = array();

    $notification_msgs[0]['title']="Tilo New Notification";
    $notification_msgs[0]['txt']="Tilo New Notification";
    if(isset($msg['object']['first_name']))
    {
      $notification_msgs[1]['title']="New Connection";
    $notification_msgs[1]['txt']=$msg['object']['first_name']." , ".$msg['object']['last_name']." has connected with you on Tilo!";

    $notification_msgs[2]['title']="Connection Request Accepted";
    $notification_msgs[2]['txt']=$msg['object']['first_name']." , ".$msg['object']['last_name']."  accepted your connection request!";;
    $notification_msgs[7]['title']="Appointment Request Accepted";
    $notification_msgs[7]['txt']=$msg['object']['first_name']." , ".$msg['object']['last_name']." has accepted your appointment request.";

    $notification_msgs[8]['title']="Appointment Request Rejected";
    $notification_msgs[8]['txt']=$msg['object']['first_name']." , ".$msg['object']['last_name']." has rejected your appointment request.";
    }
    if(isset($msg['object']['business_name']))
    {
      $notification_msgs[3]['title']="Connection Request";
      $notification_msgs[3]['txt']=$msg['object']['first_name']." , ". $msg['object']['last_name']." from ".$msg['object']['business_name']." has requested to connect with you.";


      $notification_msgs[5]['title']="Appointment Request";
      $notification_msgs[5]['txt']=$msg['object']['first_name']." , ". $msg['object']['last_name']." from ".$msg['object']['business_name']." has sent you an appointment request.";;
    }

    $notification_msgs[4]['title']="Tilo New Message";
    $notification_msgs[4]['txt']="Tilo New Message";

    $notification_msgs[6]['title']="Tilo New Notification";
    $notification_msgs[6]['txt']="Tilo New Notification";



    $notification_msgs[9]['title']="Tilo - Account Deleted";
    $notification_msgs[9]['txt']="Delete Account";
    $result = "";
    $this->firebaseSetNotifications($msg);
    if($device_type){

       $this->iosSendNotification($msg,$registrationIds,$notification_msgs);
    }else{
      $result = $this->androidSendNotification($msg,$registrationIds);
    }

  }
  public function androidSendNotification($msg,$registrationIds)
  {

    define( 'API_ACCESS_KEY', 'AIzaSyDILa1vO8Sv4pYH5NHk6_1YfPftWq2dsuY' );
    $fields = array
    (
    'registration_ids' => $registrationIds,
    'data'	=>$msg
    );
    $headers = array
    (
    'Authorization: key=' . API_ACCESS_KEY,
    'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch);
    curl_close( $ch );
    $result = json_decode($result);

   return $result;
  }

  public function iosSendNotification($array_msg,$registrationIds,$notification_msgs)
  {
    // My device token here (without spaces):
    //$deviceToken = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
    if(empty($registrationIds)) return;
    foreach($registrationIds as $deviceToken)
    {
      if($deviceToken!="(null)" && $deviceToken)
      {
    $deviceToken=str_replace("<","",$deviceToken);
    $deviceToken=str_replace(">","",$deviceToken);
    $deviceToken = str_replace(" ","",$deviceToken);
    //$deviceToken = $this->hex2bin($deviceToken);
    // My private key's passphrase here:
    $passphrase = 'myPrivateKey';

    // My alert message here:
    $message = $notification_msgs[$array_msg['msg_type']]['txt'];

    //badge
    $badge = 1;
    $ck_pem = file_get_contents("ck.pem");
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'dev_ck.pem');
    //stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

    // Open a connection to the APNS server
    //'ssl://gateway.sandbox.push.apple.com:2195', $err,
    $fp = stream_socket_client(
        'ssl://gateway.sandbox.push.apple.com:2195', $err,
        $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    if (!$fp)
    exit("Failed to connect: $err $errstr" . PHP_EOL);

    //echo 'Connected to APNS' . PHP_EOL;

    // Create the payload body
    $body['aps'] = array(
        'badge' => +1,
        'sound' => 'default',
    );
    if($array_msg['msg_type']){
    $body['aps'] = array(
        'alert' => $notification_msgs[$array_msg['msg_type']]['title'],
        'message' => $message,
        'badge' => +1,
        'sound' => 'default',
    );
    }

    $array_msg['object']['msg_type']=$array_msg['msg_type'];
    if(isset($array_msg['receiver_id']))
    {
     $array_msg['object']['receiver_id']=$array_msg['receiver_id'];
    }
    $body['object'] = $array_msg['object'];
    // Encode the payload as JSON
    $payload = json_encode($body);

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));

    if (!$result)
        {
          ?>

          <?php
        }
    else
        {
          ?>

          <?php
        }

    // Close the connection to the server

    fclose($fp);
    }
    }

  }
  public function getNotificationCount($array_msg)
  {
    $actor_id = 0;
    if($array_msg['msg_type']==1 || $array_msg['msg_type']==0)
    {
      $actor_id=$array_msg['user_id'];
    }else $actor_id = $array_msg['provider_id'];
    $db = new db();
    $sql = "select count(id) as notification_counter from notification_msg where actor_id='$actor_id'";
    $db->setQuery($sql);
    if($db->getQuery() && $db->getRowCount())
    {
      $rows = $db->getRowDataArray();
      return $rows[0]['notification_counter'];
    }else{ return 0;}
  }
  public function sendSMS($msg,$country_code,$phone_number)
  {
    /*include_once ( "include/NexmoMessage.php" );
    $to = $country_code.$phone_number;
    $nexmo_sms = new NexmoMessage('0310f162', '8697d36f');
    $info = $nexmo_sms->sendText( "$to", 'Tilo', "$msg" );*/

  }
  public function mobileNumberValidation($mobileNumbers)
  {
    $countries_code = array();
    $mobile_provider = array();

    if(is_array($mobileNumbers))
    {
      foreach($mobileNumbers as $key=>$number)
      {
        // remove special character
        $number = str_replace(' ', '', $number);
        $number =  preg_replace('/[^0-9\-]/', '', $number);
        // remove leading zero's
        $number = ltrim($number,'0');
        $mobileNumbers[$key]=$number;
      }
      return $mobileNumbers;
    }else{
      return false;
    }

  }
  function firebaseSetNotifications($msg)
  {

  }

}
?>
