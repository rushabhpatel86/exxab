<?php

/**
 * @author annab
 * @copyright 2014
 */

class db extends MConfig
{
  var $conn;
  var $query;
  var $sql_result;
  
  public function __construct() 
  {
    $this->conn = mysql_connect($this->db_host,$this->db_user,$this->db_pass);
    mysql_select_db($this->db_name);
    mysql_set_charset('utf8',$this->conn);
    if(!$this->conn){
      exit(0);
    }
  }
  
  public function setQuery($query)
  {
    $this->query = $query;
  }
  public function getQuery()
  {
    $this->sql_result = mysql_query($this->query);
    return $this->sql_result;
  }
  public function getRowCount()
  {
    return mysql_num_rows($this->sql_result);
  }
  public function getRowDataArray()
  {
    $dataArray = array();
    $counter =0;
    while($row = mysql_fetch_array($this->sql_result))
    {
      
      foreach($row as $key=>$value)
      {
        if(!is_int($key))
        {
          $dataArray[$counter][$key]=$value;
        }
        
      }
    $counter++;
    }
    return $dataArray;
  }
  public function getLastInsertedId()
  {
     return mysql_insert_id($this->conn);
  }
}
?>