/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(window).load(function() {
  var $ = jQuery;



    /****************NOURALLAH CODE***********************/

    /**/
    //Carousel
    function carousel() {
        var $ = jQuery;

        if ($('.std .products-grid').length) {
            var carouselBox = $('.std .products-grid');

            carouselBox.each(function () {
                var carousel = $(this).closest('.carousel-box'),
                        swipe,
                        autoplay,
                        prev,
                        next,
                        pagitation,
                        responsive = false,
                        speed = 700;

                if (carousel.data('carouselSpeed')) {
                    speed = carousel.data('carouselSpeed');
                }

                if (carousel.hasClass('no-swipe')) {
                    swipe = false;
                } else {
                    swipe = true;
                }

                if (carousel.attr('data-carousel-autoplay') == 'true') {
                    autoplay = true;
                } else {
                    autoplay = false;
                }

                if (carousel.attr('data-carousel-nav') == 'false') {
                    next = false;
                    prev = false;
                    carousel.addClass('no-nav');
                } else {
                    next = carousel.find('.carousel-next');
                    prev = carousel.find('.carousel-prev');
                    carousel.removeClass('no-nav');
                }

                if (carousel.attr('data-carousel-pagination') == 'true') {
                    pagination = carousel.find('.pagination');
                    carousel.removeClass('no-pagination');
                } else {
                    pagination = false;
                    carousel.addClass('no-pagination');
                }

                if (carousel.attr('data-carousel-one') == 'true') {
                    responsive = true;
                }

                $(this).carouFredSel({
                    onCreate: function () {
                        $(window).on('resize', function (event) {
                            event.stopPropagation();
                        });
                    },
                    scroll: {
                        items: 1
                    },
                    auto: {
                        play: autoplay,
                        timeoutDuration: speed
                    },
                    width: '100%',
                    infinite: false,
                    next: next,
                    prev: prev,
                    pagination: pagination,
                    responsive: responsive,
                    swipe: {
                        onMouse: false,
                        onTouch: swipe
                    }
                }).parents('.std').removeClass('load');
            });
        }
    }

    /**************** END NOURALLAH CODE***********************/


   /*
   $('.caroufredsel_wrapper').mouseenter(function(event){
	console.log('hover');
        event.preventDefault();

      $(this).parent().children('.home-carousel-control').removeClass('hidden');

   });

     $('.caroufredsel_wrapper').mouseleave(function(event){
	console.log('hover');
        event.preventDefault();

      $(this).parent().children('.home-carousel-control').addClass('hidden');

   });
*/
});

